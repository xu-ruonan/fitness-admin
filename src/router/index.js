import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";

Vue.use(VueRouter);

const routes = [
  // {
  //   path: '/',      //路由8080/直接跳转到首页课程管理页
  //   redirect:'/index/coach',
  // },
  {
    path: "/index", //首页
    name: "index",
    component: () => import("../views/Index.vue"),
    redirect: "/index/echarts", //进入到首页课程管理页
    //重定向，直接访问/component将会跳转该路径
    children: [
      {
        path: "echarts", //数据展示
        component: () => import("../views/echarts.vue"),
      },
      {
        path: "coach", //教练管理
        component: () => import("../views/Coach.vue"),
      },
      {
        path: "lesson", //课程管理
        component: () => import("../views/Lesson.vue"),
      },
      {
        path: "user", //用户管理
        component: () => import("../views/User.vue"),
        // meta: {
        //   keepAlive: true, //自定义属性,通过改属性动态设置保活
        // },
      },
    ],
  },
  {
    path: "/login", //管理人员登录路由
    name: "login",
    component: () => import("../views/LoginAge.vue"),
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/ceshi",
    name: "cs",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ceshi.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// 设定路由导航守卫,当用户没有登录时,不允许跳转其他页面
router.beforeEach((to, from, next) => {
  // 如果访问的是登录界面则直接放行
  if (to.path === '/login') return next()
  //获取用户页面token信息
  let token = window.sessionStorage.getItem('token')
  console.log(token,"456342541436")
  //如果token数据为null则跳转到指定路径
  if (!token) return next("/login")
  next()
})


export default router;
