import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 保存当前用户名
    // uname: null
     //保存当前用户名
     uname: sessionStorage.getItem("uname"), 
  },
  getters: {
  },
  mutations: {
    // updateUname(state,uname) {
    //   state.uname = uname
    //   window.sessionStorage.setItem("uname", uname);
    // },
     // state是vuex.state   newName是传递过来的新用户名参数
     updateName(state, newName) {
      state.uname = newName;
      // 将uname存入sessionStorage
      window.sessionStorage.setItem("uname", newName);
    },
  },
  actions: {
    // updateUname(store, uname) {
    //   // 异步获取相关数据
    //   // 如果希望修改state,则需要调用mutations来修改
    //   store.commit("updateUname", uname);
    // },
    updateName(store, uname) {
      // 异步获取相关数据
      // 如果希望修改state,则需要调用mutations来修改
      store.commit("updateName", uname);
    },
  },
  modules: {
  }
})
