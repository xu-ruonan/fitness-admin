import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入axios
import axios from 'axios'
Vue.prototype.$axios=axios
// 基础路径
// axios.defaults.baseURL = 'http://172.96.101.42:3000/'
axios.defaults.baseURL = 'http://123.57.250.216:3000/'

// 引入ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
